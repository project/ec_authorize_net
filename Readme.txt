
Payment Module  : AUTHORIZE.NET
Current Maintainers: Dave Augustus, Gordon Heydon
Original Author : Matt Westgate

Settings        : > Administer > e-Commerce Configuration -> Receipts -> Types -> Authorize.net

********************************************************************
DESCRIPTION:

Accept payments using Authorize.net as the payment gateway. 

This module uses the following APIs:
  - Advanced Integration Method (AIM)
  - Automated Recurring Billing (ARB)*
  
* requires ec_recurring

*********************************************************************
SPECIAL NOTES:

This module requires that you have an Authorize.net payment account. Visit http://www.authorize.net
for more information.

This module "works best" if you can provide a secure URL for this form to reside on. This is particularly
challenging when you are on a shared hosting provider do to security reasons. If you are unable to
provide your own SSL certificate to receive Authorize.net, you should investigate other APIs that 
they may offer. 

*********************************************************************
INITIAL SETUP:

After installing ec_authorize_net, visit admin/ecsettings/rtypes and ensure that your settings
are valid and configured properly. 

Visit admin/ecsettings/rtypes/list which lists the receipt methods you currently have enabled on
your site, including Authorize.net. Rearrange the order by dragging the + of each receipt method. 
Press UPDATE to save your priorities. 

Visit admin/ecsettings/rtypes/authorize_net and set the options that you want. If something is 
unselectable or greyed-out, then you may need to install additional modules (as is the case with
ec_recurring) OR the receipt method doesn't support the option.
