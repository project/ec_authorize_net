<?php
/**
 * @file
 * Provide integration with Authorize.net payment gateway.
 */

/**
 * Authorize.net Settings Page
 */
function ec_authorize_net_settings() {
  $form = array();

  $form['ec_authorize_net_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Use Live or Sandbox Settings'),
    '#default_value' => variable_get('ec_authorize_net_mode', 0),
    '#options' => array(t('Live'), t('Sandbox')),
    '#description' => t('You should use Live for normal store operation.'),
  );

  $form['ec_authorize_net_email_customer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Email Authorize.net Receipt'),
    '#default_value' => variable_get('ec_authorize_net_email_customer', TRUE),
    '#description' => t('If enabled, the customer will recieve a payment confirmation email from Authorize.Net. Keep in mind the ecommerce package sends it own transaction summary as well. Enabling this option is recommended because it provides the customer with an accurate confirmation of the amount you have charged.'),
  );

  $form['ec_authorize_net_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Authorize.net test mode'),
    '#default_value' => variable_get('ec_authorize_net_test', FALSE),
    '#description' => t('If enabled, transactions will be sent in test mode and cards will not be charged. All data will be logged in watchdog.'),
  );

  $form['ec_authorize_net_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Authorize.net debug mode'),
    '#default_value' => variable_get('ec_authorize_net_debug', FALSE),
    '#description' => t('If enabled, transactions will be fully logged in watchdog.'),
  );
  
  
  $form['live'] = array(
    '#type' => 'fieldset',
    '#title' => t('Live Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#description' => t('You should set this for normal operation of your store'),
  );

  $form['live']['ec_authorize_net_login'] = array(
    '#type' => 'textfield',
    '#title' => t('Login ID'),
    '#default_value' => variable_get('ec_authorize_net_login', 'Enter Your Login Id Here'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t("Enter your merchant login ID."),
    '#required' => TRUE,
  );

  $form['live']['ec_authorize_net_tran_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction key'),
    '#default_value' => variable_get('ec_authorize_net_tran_key', 'Enter Your Transaction Key Here'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t("Enter your merchant transaction key."),
    '#required' => TRUE,
  );

  $form['live']['ec_authorize_net_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorize.net processing URL for Advanced Integration Method - AIM'),
    '#default_value' => variable_get('ec_authorize_net_url', 'https://secure.authorize.net/gateway/transact.dll'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('URL of the secure payment processing page.'),
  );


  $form['live']['arb'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automated Recurring Billing Interface- ARB'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $options = array(
     FALSE => t('Disabled'),
     TRUE => t('Enabled'),
   );

  if (ec_authorize_net_checkreq_arb()) {
    $form['live']['arb']['ec_authorize_net_arb'] = array(
      '#type' => 'radios',
      '#title' => t('Automatic Recurring Billing'),
      '#default_value' => variable_get('ec_authorize_net_arb', 0),
      '#options' => $options,
      '#description' => t('You must contact Authorize.net to have this feature enabled on your payment gateway account. ARB allows you to receive payments on a recurring basis. Using the ec_recurring module, you can create prodcuts or services that use this feature.'),
    );
    $form['live']['arb']['ec_authorize_net_arb_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Authorize.net processing URL for ARB'),
      '#default_value' => variable_get('ec_authorize_net_arb_url', 'https://api.authorize.net/xml/v1/request.api'),
      '#size' => 70,
      '#maxlength' => 180,
      '#description' => t('URL of the secure payment processing interface for ARB'),
    );
    $form['live']['arb']['ec_authorize_net_arb_md5hash'] = array(
      '#type' => 'textfield',
      '#title' => t('MD5 Hash needed to authenticate ARB responses to web site.'),
      '#default_value' => variable_get('ec_authorize_net_arb_md5hash', ''),
      '#size' => 70,
      '#maxlength' => 180,
      '#description' => t('ARB transactions occur automatically on Authorize.net servers. When they occur, you will recieve the response encoded with this MD5 hash that you setup on your Authorize.net account.'),
    );
  }
  else {
    $form['live']['arb']['requirements'] = array(
        '#type' => 'markup',
        '#value' => t('This feature requires the XMLWriter PHP extension. Please contact your hosting provider for further assistance.'),
        '#title' => t('Requirements'),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
    );
  }



$form['sandbox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sandbox Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['sandbox']['ec_authorize_net_login_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Login ID'),
    '#default_value' => variable_get('ec_authorize_net_login_sandbox', ''),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t("Enter your merchant login ID for your TEST account."),
  );

  $form['sandbox']['ec_authorize_net_tran_key_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Transaction key'),
    '#default_value' => variable_get('ec_authorize_net_tran_key_sandbox', ''),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t("Enter your merchant transaction key for your TEST account."),
  );

  $form['sandbox']['aim']['ec_authorize_net_url_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Authorize.net processing URL for AIM testing'),
    '#default_value' => variable_get('ec_authorize_net_url_sandbox', 'https://test.authorize.net/gateway/transact.dll'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('Test URL of the secure payment processing page.'),
  );

  $form['sandbox']['arb']['ec_authorize_net_arb_url_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Authorize.net processing URL for ARB testing'),
    '#default_value' => variable_get('ec_authorize_net_arb_url_sandbox', 'https://apitest.authorize.net/xml/v1/request.api'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('Test URL of the secure payment processing interface for ARB.'),
  );
  $form['sandbox']['arb']['ec_authorize_net_arb_md5hash_sandbox'] = array(
    '#type' => 'textfield',
    '#title' => t('MD5 Hash needed to authenticate ARB responses to web site.'),
    '#default_value' => variable_get('ec_authorize_net_arb_md5hash_sandbox', ''),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('ARB transactions occur automatically on Authorize.net servers. When they occur, you will recieve the response encoded with this MD5 hash that you setup on your Authorize.net account.'),
  );

  return system_settings_form($form);
}

